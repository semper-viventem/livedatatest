package ru.semper_viventem.livecycletest.presentation

import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import ru.semper_viventem.livecycletest.R
import ru.semper_viventem.livecycletest.data.exception.ErrorReceiver
import ru.semper_viventem.livecycletest.presentation.adapter.MainAdapter

@Suppress("DEPRECATION")
/**
 * @author Kulikov Konstantin
 * @since 04.10.2017.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var mViewModel: MainViewModel
    private lateinit var mUi: MainActivityUi
    private lateinit var mAdapter: MainAdapter
    private lateinit var mProgress: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mUi = MainActivityUi()
        mUi.setContentView(this)
        mProgress = ProgressDialog(this)
        mProgress.setCancelable(false)

        mViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mProgress.show()

        mViewModel.data.observe(this, Observer { list ->
            if (list != null) mAdapter.setData(list)
            mProgress.hide()
        }, object : ErrorReceiver {
            override fun onError(error: Throwable) {
                mProgress.hide()
                toast(error.toString())
            }
        })

        mAdapter = MainAdapter()
        mUi.vRecyclerView.adapter = mAdapter
    }

    inner class MainActivityUi : AnkoComponent<MainActivity> {
        lateinit var vRecyclerView: RecyclerView

        override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
            relativeLayout {
                backgroundColor = resources.getColor(R.color.background)
                vRecyclerView = recyclerView {
                    layoutManager = LinearLayoutManager(this@MainActivity)
                }.lparams(
                        width = matchParent,
                        height = matchParent
                )
            }
        }
    }
}