package ru.semper_viventem.livecycletest.presentation

import android.arch.lifecycle.ViewModel
import ru.semper_viventem.livecycletest.data.CategoriesLifeData

/**
 * @author Kulikov Konstantin
 * @since 04.10.2017.
 */
class MainViewModel: ViewModel() {

    val data = CategoriesLifeData
}