package ru.semper_viventem.livecycletest.presentation.adapter

import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import ru.semper_viventem.livecycletest.entity.CategoryEntity

/**
 * @author Kulikov Konstantin
 * @since 05.10.2017.
 */
class MainAdapter : RecyclerView.Adapter<MainAdapter.MainHolder>() {

    private var mData: List<CategoryEntity> = emptyList()

    fun setData(data: List<CategoryEntity>) {
        mData = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MainHolder?, position: Int) {
        holder?.bind(mData[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder =
            MainHolder(MainHolder.HolderUi(), parent)

    override fun getItemCount(): Int = mData.size

    class MainHolder(
            private val mUi: HolderUi,
            parent: ViewGroup
    ): RecyclerView.ViewHolder(mUi.createView(AnkoContext.Companion.create(parent.context, parent))) {

        fun bind(item: CategoryEntity) {
            mUi.vTextDescription.text = item.description
            mUi.vTextTitle.text = item.name
        }

        class HolderUi: AnkoComponent<ViewGroup> {
            lateinit var vTextTitle: TextView
            lateinit var vTextDescription: TextView

            override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
                relativeLayout {
                    cardView {
                        radius = dip(5).toFloat()
                        backgroundColor = Color.WHITE

                        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                        elevation = dip(2).toFloat()

                        linearLayout {
                            orientation = LinearLayout.VERTICAL

                            vTextTitle = textView {
                                gravity = Gravity.CENTER_HORIZONTAL
                                textColor = Color.BLACK
                                padding = dip(5)
                            }

                            vTextDescription = textView {
                                gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    }.lparams(
                            width = matchParent,
                            height = wrapContent
                    ) {
                        margin = dip(5)
                    }
                }
            }
        }
    }

}