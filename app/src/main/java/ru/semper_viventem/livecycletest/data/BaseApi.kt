package ru.semper_viventem.livecycletest.data

import android.arch.lifecycle.LiveData
import okhttp3.OkHttpClient
import okhttp3.Request
import ru.semper_viventem.livecycletest.BuildConfig
import ru.semper_viventem.livecycletest.data.exception.ApiException

/**
 * @author Kulikov Konstantin
 * @since 05.10.2017.
 */
abstract class BaseApi<In, Out>(
        private val mOkHttpClient: OkHttpClient,
        private val mErrorReceiver: (exception: Throwable) -> Unit = {}
) : LiveData<Out>() {

    protected val BASE_URL = BuildConfig.BASE_URL

    abstract fun buildRequest(request: In? = null): Request

    abstract fun parse(response: String? = null): Out

    suspend fun execute(criteria: In? = null): Out {
        val response = mOkHttpClient.newCall(buildRequest(criteria)).execute()

        if (response.isSuccessful)
            return parse(response.body()!!.string())
        else
            throw ApiException(response.code(), response.message())
    }
}