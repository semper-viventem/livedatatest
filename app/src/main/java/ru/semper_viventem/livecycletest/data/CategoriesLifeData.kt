package ru.semper_viventem.livecycletest.data

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.*
import okhttp3.Request
import ru.semper_viventem.livecycletest.App
import ru.semper_viventem.livecycletest.data.exception.ErrorReceiver
import ru.semper_viventem.livecycletest.entity.CategoryEntity

/**
 * @author Kulikov Konstantin
 * @since 04.10.2017.
 */
object CategoriesLifeData : BaseApi<Void, List<CategoryEntity>>(App.mOkHttpClient) {

    private var mJob: Job? = null
    private var mErrorHandler: ErrorReceiver? = null

    fun observe(owner: LifecycleOwner?, observer: Observer<List<CategoryEntity>>?, errorReceiver: ErrorReceiver) {
        mErrorHandler = errorReceiver
        super.observe(owner, observer)
    }

    override fun onActive() {
        super.onActive()
        if (value != null) return

        mJob = launchWithErrorHandler(UI, mErrorHandler) {
            value = async(CommonPool) {
                execute()
            }.await()
        }
    }

    override fun onInactive() {
        mJob?.cancel()
        mErrorHandler = null
        super.onInactive()
    }


    override fun buildRequest(request: Void?): Request = Request.Builder()
            .url("$BASE_URL/api/getCategories/")
            .build()

    override fun parse(response: String?): List<CategoryEntity> =
            Gson().fromJson(response, object : TypeToken<List<CategoryEntity>>() {}.type)
}