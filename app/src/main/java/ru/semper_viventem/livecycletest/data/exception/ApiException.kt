package ru.semper_viventem.livecycletest.data.exception

/**
 * @author Kulikov Konstantin
 * @since 07.10.2017.
 */
class ApiException (
        val errorCode: Int,
        val errorMessage: String
) : Throwable() {
    override fun toString(): String =
            "ApiException(errorCode=$errorCode, errorMessage='$errorMessage')"
}