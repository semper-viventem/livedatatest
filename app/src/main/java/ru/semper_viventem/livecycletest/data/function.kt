package ru.semper_viventem.livecycletest.data

import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import ru.semper_viventem.livecycletest.data.exception.ErrorReceiver
import kotlin.coroutines.experimental.CoroutineContext

/**
 * @author Kulikov Konstantin
 * @since 07.10.2017.
 */

fun launchWithErrorHandler(
        context: CoroutineContext,
        errorReceiver: ErrorReceiver? = null,
        block: suspend CoroutineScope.() -> Unit
): Job = launch(context) {
    try {
        block()
    } catch (error: Throwable) {
        errorReceiver?.onError(error)
    }
}