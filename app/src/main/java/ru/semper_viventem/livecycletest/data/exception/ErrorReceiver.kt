package ru.semper_viventem.livecycletest.data.exception

/**
 * @author Kulikov Konstantin
 * @since 07.10.2017.
 */
interface ErrorReceiver {

    fun onError(error: Throwable)
}