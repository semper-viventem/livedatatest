package ru.semper_viventem.livecycletest

import android.app.Application
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import ru.semper_viventem.livecycletest.data.CategoriesLifeData

/**
 * @author Kulikov Konstantin
 * @since 04.10.2017.
 */
class App: Application() {

    companion object {
        lateinit var mOkHttpClient: OkHttpClient
    }


    override fun onCreate() {
        super.onCreate()

        mOkHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()

        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build())
        }
    }

}