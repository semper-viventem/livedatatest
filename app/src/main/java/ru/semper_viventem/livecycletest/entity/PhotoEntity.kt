package ru.semper_viventem.livecycletest.entity

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

/**
 * @author Kulikov Konstantin
 * @since 01.10.2017.
 */
class PhotoEntity {

    @SerializedName("id")
    var id: Long = 0

    @SerializedName("photo_url")
    var url: String = ""

    @SerializedName("preview_photo_url")
    var urlPreview: String = ""

    @SerializedName("owner_id")
    var ownerId: Long = 0

    @SerializedName("timestamp")
    var timestamp: Long = 0
}