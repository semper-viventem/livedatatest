package ru.semper_viventem.livecycletest.entity

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

/**
 * @author Kulikov Konstantin
 * @since 01.10.2017.
 */
class CategoryEntity {

    @SerializedName("id")
    var id: Long = 0

    @SerializedName("name")
    var name: String = ""

    @SerializedName("photo")
    var photo: PhotoEntity? = null

    @SerializedName("description")
    var description: String = ""

    @SerializedName("timestamp")
    var timestamp: Long = 0

    override fun toString(): String =
            "CategoryEntity(id=$id, name='$name', photo=$photo, description='$description', timestamp=$timestamp)"


}